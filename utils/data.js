// *GENERATED FROM https://www.generatedata.com/
export const dummyData = [
	{
		'name': 'Kyle S. Powers',
		'email': 'gravida@utmiDuis.org',
		'cust_id': 'TSF-Bank-100',
		'phone': '+91 8419 445 742',
		'address': '316-5643 Orci Avenue',
		'balance': '42049',
	},
	{
		'name': 'Stewart P. Miles',
		'email': 'auctor.velit.Aliquam@nuncnullavulputate.org',
		'cust_id': 'TSF-Bank-101',
		'phone': '+91 8579 198 283',
		'address': 'Ap #969-3281 Cursus St.',
		'balance': '40595',
	},
	{
		'name': 'Kylee L. Hutchinson',
		'email': 'est@accumsansedfacilisis.co.uk',
		'cust_id': 'TSF-Bank-102',
		'phone': '+91 8789 266 794',
		'address': '3038 Sem, St.',
		'balance': '55977',
	},
	{
		'name': 'Brooke B. Fields',
		'email': 'risus.Nunc@eget.ca',
		'cust_id': 'TSF-Bank-103',
		'phone': '+91 8906 490 169',
		'address': 'Ap #969-3751 Urna. Ave',
		'balance': '58349',
	},
	{
		'name': 'Hamish W. Kline',
		'email': 'sodales@est.org',
		'cust_id': 'TSF-Bank-104',
		'phone': '+91 8365 675 798',
		'address': 'P.O. Box 532, 9192 Ipsum Avenue',
		'balance': '18441',
	},
	{
		'name': 'Deanna Z. Rice',
		'email': 'arcu.eu@fringillacursus.com',
		'cust_id': 'TSF-Bank-105',
		'phone': '+91 8877 304 216',
		'address': 'P.O. Box 602, 1490 Magna Rd.',
		'balance': '72941',
	},
	{
		'name': 'Kaseem P. Sheppard',
		'email': 'tincidunt@MorbimetusVivamus.org',
		'cust_id': 'TSF-Bank-106',
		'phone': '+91 8735 721 726',
		'address': '8941 Justo. St.',
		'balance': '46110',
	},
	{
		'name': 'Zelenia T. Bryan',
		'email': 'arcu@purussapien.org',
		'cust_id': 'TSF-Bank-107',
		'phone': '+91 8409 122 183',
		'address': '570-7483 Elit St.',
		'balance': '40516',
	},
	{
		'name': 'Carissa W. George',
		'email': 'at.pretium.aliquet@tincidunt.edu',
		'cust_id': 'TSF-Bank-108',
		'phone': '+91 8781 172 712',
		'address': 'Ap #259-5703 Ac, Rd.',
		'balance': '36581',
	},
	{
		'name': 'Shannon V. Figueroa',
		'email': 'ac@in.co.uk',
		'cust_id': 'TSF-Bank-109',
		'phone': '+91 8557 618 600',
		'address': '781-3131 Nulla. Rd.',
		'balance': '0230',
	},
];

export const headings = [
	{
		head: 'Customer ID',
		key: 'cust_id',
	},
	{
		head: 'Name',
		key: 'name',
	},
	{
		head: 'Action',
		key: 'transfer',
	},
	{
		head: 'Details',
		key: 'view',
	},
];

export const transactionHeadings = [
	{
		head: 'Transaction ID',
		key: 'key',
	},
	{
		head: 'From',
		key: 'payer',
	},
	{
		head: 'To',
		key: 'reciever',
	},
	{
		head: 'Amount',
		key: 'amount',
	},
	{
		head: 'Transacted On',
		key: 'time',
	},
];
