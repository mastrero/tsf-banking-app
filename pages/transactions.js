import { useCallback, useEffect, useState } from 'react';
import { Box, Button, Spinner } from '@chakra-ui/core';
import { Table, THead, TBody } from '../_components/_table';
import { transactionHeadings } from '../utils/data';
import { useRouter } from 'next/router';

export default function Transactions() {
	const [transactionData, setTransactionData] = useState([]);
	const [loading, setLoading] = useState(true);
	const router = useRouter();

	const fetcher = useCallback(() => {
		getData();
		let count = 0;
		let fetchData = setInterval(getData, 5000);
		async function getData() {
			await fetch('/api/transactions/all')
				.then((r) => r.json())
				.then(({ status, data }) => {
					if (status === 'success') {
						setTransactionData(JSON.parse(data));
						setLoading(false);
						clearInterval(fetchData);
					} else {
						count += 1;
						if (count >= 5) {
							setLoading(false);
							clearInterval(fetchData);
						}
					}
				});
		}
	}, []);

	useEffect(() => {
		fetcher();
	}, [fetcher]);
	return (
		<Box w={['96%', '92%', '80%']} m='15px auto 0px'>
			<Box
				mb='15px'
				display='flex'
				flexDirection='row'
				flexWrap='wrap'
				justifyContent='space-between'
				alignItems='center'>
				<Box as='h2' fontWeight='500' fontSize='20px'>
					All Transactions
				</Box>
				<Button bg='#197BBD' color='white' _hover={{ opacity: 0.75 }} onClick={() => router.push('/customers')}>
					Customers
				</Button>
			</Box>
			{loading ? (
				<Box textAlign='center'>
					<Spinner size='xl' color='black' speed='0.95s' thickness='3px' />
				</Box>
			) : (
				<Table>
					<THead headings={transactionHeadings} />
					<TBody headings={transactionHeadings} contents={transactionData} />
				</Table>
			)}
		</Box>
	);
}
