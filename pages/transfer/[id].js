import { Box, Spinner, InputGroup, Input, InputLeftElement, Select, Button } from '@chakra-ui/core';
import { useCallback, useEffect, useState } from 'react';
import { BiTransferAlt } from 'react-icons/bi';
import { RiArrowDownSFill } from 'react-icons/ri';
import { useRouter } from 'next/router';

export default function Transfer() {
	const [allIds, setallIds] = useState([]);
	const [currentUser, setCurrentUser] = useState({});
	const [loading, setLoading] = useState(true);

	const router = useRouter();

	const fetchAllIds = useCallback(() => {
		getData();
		let count = 0;
		let fetchData = setInterval(getData, 5000);
		async function getData() {
			await fetch('/api/customers/all')
				.then((r) => r.json())
				.then(({ status, data }) => {
					if (status === 'success') {
						const dist = JSON.parse(data);
						setCurrentUser(() => dist.filter((d) => d.cust_id === router.query.id)[0]);
						setallIds(() => dist.map((d) => ({ id: d.cust_id, name: d.name, key: d.key })));
						setLoading(false);
						clearInterval(fetchData);
					} else {
						count += 1;
						if (count >= 5) {
							setLoading(false);
							clearInterval(fetchData);
						}
					}
				});
		}
	}, []);

	useEffect(() => {
		fetchAllIds();
	}, []);

	return loading ? (
		<Box textAlign='center'>
			<Spinner size='xl' color='black' speed='0.95s' thickness='3px' />
		</Box>
	) : (
		<Box w={['94%', '90%', '50%']} borderRadius='3px' bg='white' boxShadow='0 1px 4px 0 #7b7b7b' m='0 auto'>
			<Transfer_box currentUser={currentUser} allIds={allIds} />
		</Box>
	);
}

const Transfer_box = ({ currentUser, allIds }) => {
	const router = useRouter();

	const [paymentDetails, setPaymentDetails] = useState({
		amount: ``,
		to: ``,
	});
	const [errors, setError] = useState({
		amount: false,
		to: false,
	});
	const [processing, setProcessing] = useState(false);
	const [done, setDone] = useState(false);

	const updateDetails = ({ target: { name, value } }) => {
		setError(() => ({ amount: false, to: false }));
		setPaymentDetails((prev) => ({ ...prev, [name]: value }));
	};

	const makePayment = async (e) => {
		e.preventDefault();
		if (paymentDetails.amount === '') setError((prev) => ({ ...prev, amount: true }));
		else if (paymentDetails.to === '') setError((prev) => ({ ...prev, to: true }));
		else {
			const data = { from: currentUser.key, ...paymentDetails };
			setProcessing(true);
			pay();
			let count = 0;
			let timer = setInterval(pay, 5000);
			async function pay() {
				await fetch('/api/payment', {
					method: 'POST',
					body: JSON.stringify(data),
				})
					.then((r) => r.json())
					.then((data) => {
						if (data.status === 'done') {
							setDone(true);
							setTimeout(() => {
								router.push('/customers');
							}, 3000);
							setProcessing(false);
							clearInterval(timer);
						} else {
							count += 1;
							if (count >= 5) {
								setProcessing(false);
								clearInterval(timer);
							}
						}
					});
			}
		}
	};

	const modal = (
		<>
			<Box
				as='h2'
				w='100%'
				fontSize='20px'
				mb='10px'
				fontWeight='500'
				display='flex'
				justifyContent='center'
				alignItems='center'
				textAlign='center'>
				<Box as={BiTransferAlt} />
				Make Transaction
			</Box>
			<Box>
				<Box as='h3' fontSize='16px'>
					Customer name : <span style={{ fontWeight: '500', fontStyle: 'italic' }}>{currentUser.name}</span>
				</Box>
				<Box as='h3' fontSize='16px'>
					Customer ID : <span style={{ fontWeight: '500', fontStyle: 'italic' }}>{currentUser.cust_id}</span>
				</Box>
				<Box as='h3' fontSize='16px'>
					Available Balance : ₹ <span style={{ fontWeight: '500' }}>{currentUser.balance}</span>
				</Box>
				<Box w={['100%', '70%', '50%']} m='15px auto 0'>
					<InputBox value={paymentDetails.amount} update={updateDetails} error={errors.amount} />
					<SelectBox
						currentUserKey={currentUser.key}
						allIds={allIds}
						value={paymentDetails.to}
						update={updateDetails}
						error={errors.to}
					/>
					<Button w='50%' ml='50%' onClick={makePayment} bg='#00C9A7' color='white' _hover={{ opacity: 0.75 }}>
						{!processing ? `Transfer` : `Transferring ...`}
					</Button>
				</Box>
			</Box>
		</>
	);

	return (
		<Box w='100%' h='100%' p={`15px`} bg={done ? `#f7f7f7` : `white`}>
			{done ? <TransferDone /> : modal}
		</Box>
	);
};

const InputBox = ({ value, update, error }) => (
	<InputGroup size='sm' mb='10px'>
		<InputLeftElement children='₹' />
		<Input
			type='number'
			focusBorderColor='grey'
			borderColor='grey'
			isInvalid={error}
			errorBorderColor='red'
			placeholder='Amount'
			onChange={update}
			isRequired={true}
			name='amount'
			value={value}
		/>
	</InputGroup>
);

const SelectBox = ({ allIds = [], value, update, error, currentUserKey }) => (
	<InputGroup size='sm' mb='10px'>
		<Select
			icon={RiArrowDownSFill}
			focusBorderColor='grey'
			borderColor='grey'
			isInvalid={error}
			errorBorderColor='red'
			value={value}
			onChange={update}
			iconSize={10}
			placeholder={`Transfer to`}
			isRequired={true}
			name='to'>
			{allIds.map(({ id, name, key }) =>
				currentUserKey !== key ? (
					<Box as='option' fontSize='16px' key={key} value={key}>
						{`${id} | ${name}`}
					</Box>
				) : null
			)}
		</Select>
	</InputGroup>
);

const TransferDone = () => (
	<Box w={['100%', '92%', '50%']} m='0 auto' textAlign='center'>
		<Box
			as='img'
			src='https://i.pinimg.com/originals/35/f3/23/35f323bc5b41dc4269001529e3ff1278.gif'
			alt='done'
			title='done'
			w='100%'
		/>
		<Box as='h2' fontWeight='500' fontSize='20px' my='10px'>
			Transaction Successful
		</Box>
		<Box as='h3' fontSize='14px' fontStyle='italic'>
			Redirecting ...
		</Box>
	</Box>
);
