import Head from 'next/head';
import ThemeProvider from '../styles/theme';
import Topbar from '../_components/_navbar';

function MyApp({ Component, pageProps }) {
	return (
		<>
			<Head>
				<meta charset='utf-8' />
				<meta http-equiv='X-UA-Compatible' content='IE=edge' />
				<link rel='icon' href='/favicon.ico' />
				<link rel='manifest' href='/manifest.json' />
				<link href='/logo16.png' rel='icon' type='image/png' sizes='16x16' />
				<link href='/logo32.png' rel='icon' type='image/png' sizes='32x32' />
				<link rel='apple-touch-icon' href='/apple-icon.png'></link>
				<link
					href='https://fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,500;0,600;1,400;1,500;1,600&display=swap'
					rel='stylesheet'
				/>
				<meta name='theme-color' content='#30333c' />
				<meta
					name='viewport'
					content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no'
				/>
				<meta name='description' content='Challange for The Sparks Foundation' />
				<meta name='keywords' content='react, spark, nextjs, chakra, ui, banking system' />
				<title>Banking System | TSF</title>
			</Head>
			<ThemeProvider>
				<Topbar />
				<Component {...pageProps} />
			</ThemeProvider>
		</>
	);
}

export default MyApp;
