const { Deta } = require('deta');

export default async (req, res) => {
	const { from, to, amount } = JSON.parse(req.body);

	const deta = Deta(process.env.DETA_KEY);
	const customersDb = deta.Base('customers');
	const transactionsDb = deta.Base('transactions');
	try {
		const { balance: fromBal, cust_id: fromId } = await customersDb.get(from);
		const { balance: toBal, cust_id: toId } = await customersDb.get(to);
		await customersDb.update(
			{
				balance: (parseFloat(fromBal) - parseFloat(amount)).toFixed(2),
				parseFloat,
			},
			from
		);
		await customersDb.update(
			{
				balance: (parseFloat(toBal) + parseFloat(amount)).toFixed(2),
			},
			to
		);

		await transactionsDb.put({
			amount,
			payer: fromId,
			reciever: toId,
			time: new Date().getTime(),
		});

		return res.status(200).json({ status: 'done' });
	} catch (e) {
		return res.status(200).json({ status: 'error' });
	}
};
