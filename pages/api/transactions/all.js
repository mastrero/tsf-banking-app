const { Deta } = require('deta');

export default async (req, res) => {
	const deta = Deta(process.env.DETA_KEY);
	const transactionsDb = deta.Base('transactions');
	const { value: data } = await transactionsDb.fetch().next();

	if (data.length !== 0) {
		return res.status(200).json({ status: 'success', data: JSON.stringify(data) });
	}
	return res.status(200).json({ status: 'error' });
};
