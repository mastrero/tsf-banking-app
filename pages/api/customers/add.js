const { Deta } = require('deta');

export default async (req, res) => {
	const deta = Deta(process.env.DETA_KEY);
	const customersDb = deta.Base('customers');
	await customersDb.put(JSON.parse(req.body));

	return res.status(200).json({ status: 'ADDED' });
};
