import { Box, Button } from '@chakra-ui/core';
import { RiBankLine } from 'react-icons/ri';
import { BsArrowRightShort } from 'react-icons/bs';
import { useRouter } from 'next/router';

export default function Index() {
	const router = useRouter();

	return (
		<Box w='100%' h='100%'>
			<Box display='flex' mt='50px' flexDirection='column' justifyContent='center' alignItems='center'>
				<Box as={RiBankLine} w='40px' h='40px' />
				<Box as='h2' fontSize='30px' fontWeight='400'>
					Banking made easy
				</Box>
				<Box as='h3' fontSize='24px'>
					just for you
				</Box>
				<Button
					onClick={() => router.push('/customers')}
					mt='30px'
					py='5px'
					h=''
					bg='white'
					color='dark'
					borderRadius='5px'
					display='flex'
					flexDirection='column'
					justifyContent='center'
					fontSize='14px'
					_hover={{
						opacity: 0.85,
					}}>
					Continue
					<Box as={BsArrowRightShort} w='22px' h='22px' verticalAlign='middle' textAlign='center' />
				</Button>
			</Box>
		</Box>
	);
}
