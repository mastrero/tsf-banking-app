import { Box, Text, Link, Icon } from '@chakra-ui/core';

const Topbar = () => {
	return (
		<Box as='header' w='100%' h='46px' color='black' mb='8px' boxShadow='1px 0 5px 0 #525252'>
			<Box
				w={['94%', null, '84%']}
				h='100%'
				m='0 auto'
				display='flex'
				alignItems='center'
				justifyContent='space-between'>
				<Text as='h1' fontSize='22px' fontWeight='400'>
					TSF - Banking System
				</Text>
				<Link
					href='https://www.thesparksfoundationsingapore.org/'
					style={{
						display: 'flex',
						alignItems: 'center',
					}}
					isExternal
					fontSize='18px'>
					TSF <Icon name='external-link' mx='5px' />
				</Link>
			</Box>
		</Box>
	);
};
export default Topbar;
