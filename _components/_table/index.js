import { Box, Button } from '@chakra-ui/core';
import Link from 'next/link';
import { AiOutlineFullscreen } from 'react-icons/ai';
import { forwardRef } from 'react';
import { BiSend } from 'react-icons/bi';
import {
	Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
	useDisclosure,
} from '@chakra-ui/core';

export const Table = ({ children }) => (
	<Box overflowX='auto' maxH='78vh'>
		<Box as='table' w='100%' minW='600px' h='10%'>
			{children}
		</Box>
	</Box>
);

export const THead = ({ headings }) => (
	<Box as='thead'>
		<Box as='tr'>
			{headings.map(({ head, key }) => (
				<Box
					pr='5px'
					boxShadow='5px 0 5px 0 #525252'
					fontWeight='500'
					as='th'
					key={key}
					textAlign='left'
					position='sticky'
					top='0'
					zIndex='10'
					bg='white'>
					{head || `HEADING N/A`}
				</Box>
			))}
		</Box>
	</Box>
);

export const TBody = ({ contents, headings }) => (
	<Box as='tbody' mt='5px'>
		{contents.map((row, index) => {
			return (
				<Box as='tr' key={`${index}${row.key || ''}`} h='30px'>
					{headings.map(({ key }) => (
						<Box
							as='td'
							pr='5px'
							key={key}
							whiteSpace='nowrap'
							color={key === 'cust_id' || key === 'key' ? 'red' : 'black'}
							fontWeight={key === 'cust_id' || key === 'key' ? '500' : 'normal'}>
							{key === 'transfer' ? (
								<TransferAction id={row['cust_id']} />
							) : key === 'view' ? (
								<ViewDetails data={row} />
							) : key === 'amount' ? (
								<Box as='span' fontWeight='500'>{`₹ ${row[key]}`}</Box>
							) : key === 'time' ? (
								<TimeFormat time={row[key]} />
							) : (
								row[key] || 'N/A'
							)}
						</Box>
					))}
				</Box>
			);
		})}
	</Box>
);

const TimeFormat = ({ time }) => {
	const t = new Date(time);
	const t1 = `${t.getDate()}/${t.getMonth()}/${t.getFullYear()}`;
	const hours = t.getHours() < 12 ? t.getHours() : t.getHours() - 12;
	const t2 = `${hours <= 9 ? `0${hours}` : hours}:${t.getMinutes()}:${
		t.getSeconds() <= 9 ? `0${t.getSeconds()}` : t.getSeconds()
	}`;
	const am_pm = t.getHours() > 12 ? `PM` : `AM`;

	return <Box as='span'>{`${t1} ${t2} ${am_pm}`}</Box>;
};

const TransferAction = ({ id }) => (
	<Box
		as={Link}
		href={{
			pathname: '/transfer/[id]',
			query: { id: id },
		}}
		textAlign='left'
		h='25px'
		color='red'
		_hover={{ opacity: 0.75 }}
		display='flex'
		alignItems='center'
		passHref>
		<LINK />
	</Box>
);
const LINK = forwardRef(({ onClick, href }, ref) => (
	<Box
		as='a'
		href={href}
		onClick={onClick}
		ref={ref}
		cursor='pointer'
		display='flex'
		flexWrap='nowrap'
		alignItems='center'
		fontWeight='500'
		color='#354A5C'>
		Transfer{BiSend()}
	</Box>
));

const ViewDetails = ({ data }) => {
	const { address, cust_id, name, email, phone, balance } = data;
	const { isOpen, onOpen, onClose } = useDisclosure();
	return (
		<>
			<Button
				textAlign='left'
				h='100%'
				p='0 10px'
				m='0'
				bg='#de6b42'
				color='white'
				_hover={{ opacity: 0.75 }}
				lineHeight='0'
				onClick={onOpen}>
				view&nbsp;{AiOutlineFullscreen()}
			</Button>
			<Modal blockScrollOnMount={false} isOpen={isOpen} onClose={onClose}>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>Customer Details</ModalHeader>
					<ModalCloseButton />
					<ModalBody>
						<Box>
							{`Customer ID : `}
							<Box as='span' fontWeight='500'>
								{cust_id}
							</Box>
						</Box>
						<Box>
							{`Name : `}
							<Box as='span' fontWeight='500' fontSize='18px'>
								{name}
							</Box>
						</Box>
						<Box>
							{`Email : `}
							<Box as='span' fontWeight='500'>
								{email}
							</Box>
						</Box>
						<Box>
							{`Phone : `}
							<Box as='span' fontWeight='500'>
								{phone}
							</Box>
						</Box>
						<Box>
							{`Address : `}
							<Box as='span' fontWeight='500'>
								{address}
							</Box>
						</Box>
						<Box>
							{`Balance : `}
							<Box as='span' fontWeight='500'>
								{`₹ ${balance}`}
							</Box>
						</Box>
					</ModalBody>

					<ModalFooter>
						<Button color='white' bg='red' mr={3} onClick={onClose}>
							Close
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};
