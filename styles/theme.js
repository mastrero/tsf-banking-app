import { theme, ThemeProvider as TP, CSSReset } from '@chakra-ui/core';
import { Global, css } from '@emotion/core';

export const customTheme = {
	...theme,
	colors: {
		...theme.colors,
		dark: '#30333c',
		bleach: '#ededed',
		black: '#000000',
		white: '#ffffff',
		red: '#e93558',
		blue: '#00c7c7',
		yellow: '#fff660',
	},
};

const ThemeProvider = ({ children }) => (
	<TP theme={customTheme}>
		<CSSReset />
		<Global
			styles={css`
				*,
				html,
				body {
					font-family: 'Lora', -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif,
						'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
				}
			`}
		/>
		{children}
	</TP>
);

export default ThemeProvider;
